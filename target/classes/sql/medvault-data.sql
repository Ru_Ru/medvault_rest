-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.23-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Dumping data for table medvault.medication: ~4 rows (approximately)
/*!40000 ALTER TABLE `medication` DISABLE KEYS */;
INSERT INTO `medication` (`id`, `name`, `description`, `img`, `type`) VALUES
	(1, 'Tylenol Cold Congestion', 'Cold and Flu treatment', '', 'Capsules'),
	(2, 'PhysiciansCare Cold and Cough Congestion Medication', 'Cold and Flu treatment', '', 'Syrup'),
	(3, 'Vicks Dayquil Multisymptom Cold & Flu Relief LiquiCaps, Non-Drowsy', 'For serious cold and soar throat relief', '', 'Tea Powder'),
	(4, 'Shop Dristan Cold Tablets', 'Cold and flu relief', '', 'Capsules');
/*!40000 ALTER TABLE `medication` ENABLE KEYS */;

/*
* PostgresSql Insert "Medication" *
INSERT INTO medication (id, name, description, img, type) VALUES
	(1, 'Tylenol Cold Congestion', 'Cold and Flu treatment', '', 'Capsules'),
	(2, 'PhysiciansCare Cold and Cough Congestion Medication', 'Cold and Flu treatment', '', 'Syrup'),
	(3, 'Vicks Dayquil Multisymptom Cold & Flu Relief LiquiCaps, Non-Drowsy', 'For serious cold and soar throat relief', '', 'Tea Powder'),
	(4, 'Shop Dristan Cold Tablets', 'Cold and flu relief', '', 'Capsules');

*/
-- Dumping data for table medvault.medication_symptom: ~7 rows (approximately)
/*!40000 ALTER TABLE `medication_symptom` DISABLE KEYS */;
INSERT INTO `medication_symptom` (`medications_id`, `symptoms_id`) VALUES
	(2, 2),
	(4, 2),
	(1, 2),
	(1, 1),
	(3, 2),
	(3, 1),
	(4, 5);
/*!40000 ALTER TABLE `medication_symptom` ENABLE KEYS */;

-- Dumping data for table medvault.prescription: ~0 rows (approximately)
/*!40000 ALTER TABLE `prescription` DISABLE KEYS */;
/*!40000 ALTER TABLE `prescription` ENABLE KEYS */;

-- Dumping data for table medvault.prescription_medication: ~0 rows (approximately)
/*!40000 ALTER TABLE `prescription_medication` DISABLE KEYS */;
/*!40000 ALTER TABLE `prescription_medication` ENABLE KEYS */;

-- Dumping data for table medvault.symptom: ~7 rows (approximately)
/*!40000 ALTER TABLE `symptom` DISABLE KEYS */;
INSERT INTO `symptom` (`id`, `info`, `name`) VALUES
	(1, NULL, 'Cold and  Flu'),
	(2, NULL, 'Headache'),
	(3, NULL, 'Depression'),
	(4, NULL, 'Low Blood Pressure'),
	(5, NULL, 'High Blood Pressure'),
	(6, NULL, 'Kidney Failure'),
	(7, NULL, 'Stomach Ache');
/*!40000 ALTER TABLE `symptom` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


/*

** Symtoms + Relations **
INSERT INTO symptom (id, info, name) VALUES
	(1, NULL, 'Cold and  Flu'),
	(2, NULL, 'Headache'),
	(3, NULL, 'Depression'),
	(4, NULL, 'Low Blood Pressure'),
	(5, NULL, 'High Blood Pressure'),
	(6, NULL, 'Kidney Failure'),
	(7, NULL, 'Stomach Ache');


INSERT INTO medication_symptom (medications_id, symptoms_id) VALUES
	(2, 2),
	(4, 2),
	(1, 2),
	(1, 1),
	(3, 2),
	(3, 1),
	(4, 5);*/
