/**
 * Created by Ruslan on 20/11/2015.
 */

(function () {
    "user strict";

    angular.module("ui-helper", [])
        .directive("waitCursor", waitCursor);

    function waitCursor() {
        return {
            scope: {
                show: "=displayWhen"
            },
            restrict: "E",
            templateUrl: "/medvault/app/views/waitCursor/waitCursor.html"
        };
    }

})();