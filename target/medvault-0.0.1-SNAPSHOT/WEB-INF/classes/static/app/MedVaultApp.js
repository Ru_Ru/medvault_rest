(function () {

    "use strict";

    // var medvault = angular.module("MedVault", ['ngRoute']);
    var app = angular.module("MedVault", [
        'ngRoute', 'ui-select', 'ngSanitize', 'ui-helper'
    ])
        .config(function ($routeProvider) {
            //$locationProvider.html5Mode(true);
            //$routeProvider
            $routeProvider
                .when('/catalog', {
                    templateUrl: '/medvault/app/views/medication-catalog/catalog.html',
                    controller: 'CatalogCtrl'
                })

                .when('/login', {
                    templateUrl: '/medvault/app/views/login/login.html',
                    controller: 'LoginCtrl'
                })

                .when('/singup', {
                    templateUrl: '/medvault/app/views/register/signup.html',
                    controller: 'SingUpCtrl'
                })

                .when('/create/medication', {
                    templateUrl: '/medvault/app/views/admin-create-med/create-med.html',
                    controller: 'MedCtrl'
                })
                .when('/create/prescription', {
                    templateUrl: '/medvault/app/views/admin-create-prescription/create.html',
                    controller: 'PrescCtrl'
                })

                .otherwise({
                    redirectTo: '/index'
                });
        });
})();
