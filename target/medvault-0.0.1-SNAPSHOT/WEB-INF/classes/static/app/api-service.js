/**
 * Created by Ruslan on 14/11/2015.
 */

(function(){

    angular.module("MedVault").factory('dataSource', ['$q', '$http', '$cacheFactory', dataSource]);

    function dataSource($q, $http, $cacheFactory){

        self.medicationsCache = DSCacheFactory.get("medicationCache");
        self.medicationDataCache = DSCacheFactory.get("medicationDetailsCache");

        function getMedications(){
            var deferred = $q.defer(),
                cacheKey = "medications",
                medicationsData = self.medicationsCache.get(cacheKey);

            if(medicationsData) {
                console.log("Found data inside cache", medicationsData);
                deferred.resolve(medicaionsData)
            } else {
                $http.get("/medvault/api/medication/search/all")
                    .then(function(response){
                    console.log("Received schedule data via HTTP.");
                    self.medicationsCache.put(cacheKey, data);
                        console.log(result);
                    deferred.resolve(response);
                }, function(error){
                    console.log("Error while making HTTP call.");
                    deferred.reject();
                });
            }
        }
    }
})();
