# README #

**MEDVAULT**

MedVault is a mobile applications that allows a user to track their personal prescriptions and medications.
Providing schedualing, alarms, and a catalogue for searching medications easily and freely through a mobile interface.

**REST API**

MedVault communicates with the server through the traditional REST API, which provides all the relevant resources
to the mobile device.