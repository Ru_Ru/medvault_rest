/**
 * Created by Ruslan on 14/11/2015.
 */

(function () {

    "use strict";

    angular.module("MedVault").controller("CatalogCtrl", ['$http', CatalogCtrl]);

    function CatalogCtrl($http) {

        var vm = this;
        //vm.getMedications();
        vm.isBusy = true;
        vm.catalog = [];
        vm.errorText = "";

        vm.message = "Medication Catalog";

            $http.get("/medvault/api/medication/search/all")
                .then(function (response) {
                    angular.copy(response.data, vm.catalog);
                    console.log(vm.catalog);
                }, function (error) {
                    vm.errorText = "Error: " + error;
                    console.log(vm.catalog)
                })
                .finally(function () {
                    console.log(vm.isBusy);
                    vm.isBusy = false;
                });


        vm.deleteMedication = function (medId) {
            $http.post("/medvault/api/medication/delete", medId)
                .then(function (response) {
                    //$http.get("/medvault/api/medication/search/all").then(function(response){
                    //   angular.copy(response.data, vm.catalog)
                    //});
                    vm.getMedications();
                    vm.catalog.push(response.data);
                    vm.newMed = {}
                }, function (error) {
                    vm.errorText = "Error : " + error;
                }).finally(function () {
                    vm.isBusy = false
                });
        }
    }

})();
