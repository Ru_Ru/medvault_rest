/**
 * Created by Ruslan on 16/11/2015.
 */

(function(){

  'use strict'

  app.controller("PrescCtrl", PrescCtrl);

  function PrescCtrl($http, $log){
    var vm = this;
    vm.isBuys = true;

    vm.title = "Create Prescription Form";

    $http.get("/medvault/prescriptions")
      .then(function(response){
          $log.info(response.data)
    }, function(error){
        $log.error("Error:" + error)
    });

 vm.createPresc = function() {

    $http.post("/medvault/api/medication/create", vm.newPresc)
      .then(function(response) {
        //TODO:: Send the object, and clear the object. Maybe Move
        $log.info(vm.newPresc);
        $log.info(response);

        vm.newPresc = {};
      }, function(error){
        $log.error("Error happed during post: " + error)
      }).finally(function(){
        vm.isBusy = false;
      });
    }
  }

})();
