/**
 * Created by Ruslan on 16/11/2015.
 */

(function(){

    "use strict";

    angular.module("MedVault").controller("MedCtrl", MedCtrl);

    function MedCtrl($http, $location){

        var vm = this;

        vm.newMed = {};
        vm.message= "";

        vm.symptoms = [];

        $http.get("/medvault/api/symptoms/all")
            .then(function (response) {
                angular.copy(response.data, vm.symptoms);
                console.log(vm.symptoms)
            }, function (error) {
                vm.message = "Error: " + error;
                console.log(vm.catalog)
            });

        vm.createMed = function() {
            console.log(vm.newMed);
            vm.isBusy = true;

            $http.post("/medvault/api/medication/create", vm.newMed)
                .then(function(response){
                    $location.path("/catalog");
                    console.log(vm.newMed);
                    vm.newMed = {}
                }, function(error){
                    vm.message = "Error : " + error;
                    console.log(vm.newMed)
                }).finally(function () {
                    vm.isBusy = false

                });

        }
    }
})();