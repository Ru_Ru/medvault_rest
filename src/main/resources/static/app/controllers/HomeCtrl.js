/**
 * Created by Ruslan on 09/11/2015.
 */
(function(){

    "use strict";

    angular.module("MedVault").controller("HomeCtrl", ['$scope', HomeCtrl]);

    function HomeCtrl($scope, store) {

        var vm = this;

        //vm.$route = $route;
        //vm.$location = $location;
        //vm.$routeParams = $routeParams;

        vm.hello = "Hello World From Angular";
    }

})();