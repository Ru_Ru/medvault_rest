package com.ruruapps.medvault;

import com.ruruapps.medvault.domain.Medication;
import com.ruruapps.medvault.domain.Prescription;
import com.ruruapps.medvault.domain.Symptom;
import com.ruruapps.medvault.domain.User;
import com.ruruapps.medvault.repositories.MedicationRepository;
import com.ruruapps.medvault.repositories.PrescriptionRepository;
import com.ruruapps.medvault.repositories.SymptomRepository;
import com.ruruapps.medvault.repositories.UserRepository;
import com.ruruapps.medvault.service.PrescriptionService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
//@ComponentScan(value = {"com.ruruapps"})
@EnableAutoConfiguration
//@Import(SpringSecurityWebAppConfig.class)
public class MedVaultApplication {

    public static void main(String[] args) {
        SpringApplication.run(MedVaultApplication.class, args);
    }

//    @Bean
//    CommandLineRunner runner(PrescriptionRepository prescriptionRepository, SymptomRepository symptomRepository, PrescriptionService prescriptionService, MedicationRepository medicationRepository) {
//        return strings -> {
//            //ArrayList<Prescription> prescriptions = (ArrayList<Prescription>) prescriptionRepository.findAll();
//
//            Prescription p1 = prescriptionRepository.findOne((long) 1);
//            Prescription p2 = prescriptionRepository.findOne((long) 2);
//
//            ArrayList<Medication> medications = (ArrayList<Medication>) medicationRepository.findAll();
//            ArrayList<Medication> p1Meds = new ArrayList<>();
//            Medication med = medicationRepository.findByName("Vitabiotics Feroglobin Vitamin and Mineral Capsules");
//
//            for (Medication medication :
//                    medications) {
//                if (medication.getName().equals("Strepsilis")) {
//                    p1Meds.add(medication);
//                }
//                if (medication.getName().equals("PhysiciansCare Cold and Cough Congestion Medication")) {
//                    p1Meds.add(medication);
//                }
//            }
//
//            prescriptionService.update(p1, p1Meds);
//            prescriptionService.update(p2, med);
//        };
//    }

//    @Bean
//    CommandLineRunner runner(UserRepository userRepository, SymptomRepository symptomRepository, PrescriptionService prescriptionService, MedicationRepository medicationRepository) {
//        return strings -> {
//            System.out.println("==== Starting Processing... ====");
//            Thread.sleep(5000);
//
//            User user = new User("John Doe", LocalDate.of(1991, 6, 14));
//            userRepository.save(user);
//
//            ArrayList<Prescription> prescriptions = new ArrayList<>();
//            ArrayList<Medication> medications = (ArrayList<Medication>) medicationRepository.findAll();
//            Symptom caught = new Symptom("Dry and Soar Throat");
//            Symptom ironDeficiency = new Symptom("Iron Deficiency");
//            symptomRepository.save(caught);
//            symptomRepository.save(ironDeficiency);
//            ArrayList<Symptom> symptoms = (ArrayList<Symptom>) symptomRepository.findAll();
//            Medication strepsil = new Medication("Strepsilis",
//                    "Sweets", "Strepsilis", "Cure for soar and dry throat");
//            strepsil.addSymptom(caught);
//
//            Medication iron = new Medication("Vitabiotics Feroglobin Vitamin and Mineral Capsules",
//                    "Tablet",
//                    "Vitabiotics-Feroglobin", "Provides high concentration of iron");
//
//            for (Symptom sym :
//                    symptoms) {
//                if (sym.getName().equals("Low Blood Pressure"))
//                    iron.addSymptom(sym);
//                iron.addSymptom(ironDeficiency);
//            }
//
//            medicationRepository.save(iron);
//            medicationRepository.save(strepsil);
//
//            ArrayList<Medication> p1Meds = new ArrayList<>();
//            p1Meds.add(strepsil);
//
//            ArrayList<Medication> p2Meds = new ArrayList<>();
//            p2Meds.add(iron);
//
//            for (Medication med :
//                    medications) {
//                if (med.getName().equals("PhysiciansCare Cold and Cough Congestion Medication")) {
//                    p1Meds.add(med);
//                }
//                if (med.getName().equals("Shop Dristan Cold Tablets")) {
//                    p1Meds.add(med);
//                }
//
//            }
//
//            Prescription p1 = new Prescription("Cold Prescription", user, p1Meds, LocalDate.now());
//            Prescription p2 = new Prescription("Heart Condition", user, p2Meds, LocalDate.of(2015, 11, 4));
//            prescriptions.add(p1);
//            prescriptions.add(p2);
//
//            prescriptions.forEach(prescriptionService::save);
//
//            //prescriptions.forEach(prescriptionRepository::save);
//
//            System.out.println(" Process Was Successful ");
//        };
//    }

//    @Bean
//    CommandLineRunner runner(MedicationRepository medicationRepository, SymptomRepository symptomRepository) {
//        return strings -> {
//            System.out.println("==== Starting Processing... ====");
//            Thread.sleep(5000);
//
//            Medication thisMed = medicationRepository.findOne((long) 3);
//            Symptom cold = symptomRepository.findOne((long)1);
//
//            thisMed.addSymptom(cold);
//
//            medicationRepository.save(thisMed);
//            System.out.println(" Process Was Successful ");
//
//        };
//    }

//    @Bean
//    CommandLineRunner runner(MedicationRepository medicationRepository, SymptomRepository symptomRepository) {
//        return strings -> {
//
//            Arrays.asList("Cold & Flu,Headache,Depression,Low Blood Pressure,High Blood Pressure,Kidney Failure,Stomach Ache"
//                    .split(",")).forEach(n -> symptomRepository.save(new Symptom(n)));
//
//            List<Medication> medicationList = new ArrayList<>();
//            List<Symptom> symptoms = new ArrayList<>();
//            symptoms.add(symptomRepository.findByName("Cold & Flu"));
//            symptoms.add(symptomRepository.findByName("Headache"));
//
//            Medication m1 = new Medication("Tylenol Cold Congestion", symptoms, "Capsules", "",
//                    "Cold and Flu treatment");
//            Medication m2 = new Medication("PhysiciansCare Cold and Cough Congestion Medication", symptoms, "Syrup", "",
//                    "Cold and Flu treatment");
//            Medication m3 = new Medication("Vicks Dayquil Multisymptom Cold & Flu Relief LiquiCaps, Non-Drowsy",
//                    symptoms, "Tea Powder",
//                    "", "For serious cold and soar throat relief");
//            Medication m4 = new Medication("Shop Dristan Cold Tablets", symptoms, "Capsules", "", "Cold and flu relief");
//
//            medicationList.add(m1);
//            medicationList.add(m2);
//            medicationList.add(m3);
//            medicationList.add(m4);
//
//            medicationList.forEach(medicationRepository::save);
//
//            Medication thisMed = medicationRepository.findOne((long) 3);
//            Symptom cold = symptomRepository.findOne((long)1);
//
//            thisMed.addSymptom(cold);
//
//            medicationRepository.save(thisMed);
//        };
//    }
}
