package com.ruruapps.medvault.repositories;

import com.ruruapps.medvault.domain.Symptom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.List;

/**
 * Created by Ruslan on 25-10-2015.
 */

@RepositoryRestResource
@PersistenceContext(type = PersistenceContextType.EXTENDED)
public interface SymptomRepository extends JpaRepository<Symptom, Long> {

    @Query("select s from Symptom s where s.name = :name")
    Symptom findByName(@Param("name") String name);
}
