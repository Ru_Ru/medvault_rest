package com.ruruapps.medvault.repositories;

import com.ruruapps.medvault.domain.Prescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by Ruslan on 21/11/2015.
 */

@RepositoryRestResource
public interface PrescriptionRepository extends JpaRepository<Prescription, Long> {

}
