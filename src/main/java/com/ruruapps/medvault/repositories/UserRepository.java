package com.ruruapps.medvault.repositories;

import com.ruruapps.medvault.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Created by Ruslan on 21/11/2015.
 */

@RepositoryRestResource
public interface UserRepository extends JpaRepository<User, Long> {

}
