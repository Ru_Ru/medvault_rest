package com.ruruapps.medvault.repositories;

import com.ruruapps.medvault.domain.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Ruslan on 01/02/2016.
 */
public interface EntitiesRepository<T> extends JpaRepository<T, Long> {


    @Query("select m from Medication m where m.type = :type")
    List<T> findByType(@Param("type") String type);

    @Query("select m from Medication m where m.name = :name")
    T findByName(@Param("name") String name);

    @Query("select m from Medication m join m.symptoms s where s.name = :symptom")
    List<T> findBySymptom(@Param("symptom") String symptom);
}
