package com.ruruapps.medvault.repositories;

import com.ruruapps.medvault.domain.Medication;
import com.ruruapps.medvault.domain.Symptom;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.Collection;
import java.util.List;

/**
 * Created by Ruslan on 25-10-2015.
 */

@RepositoryRestResource
@PersistenceContext(type = PersistenceContextType.EXTENDED)
public interface MedicationRepository extends JpaRepository<Medication, Long> {


    @Query("select m from Medication m where m.type = :type")
    List<Medication> findByType(@Param("type") String type);

    @Query("select m from Medication m where m.name = :name")
    Medication findByName(@Param("name") String name);

    @Query("select m from Medication m join m.symptoms s where s.name = :symptom")
    List<Medication> findBySymptom(@Param("symptom") String symptom);
}
