package com.ruruapps.medvault.helper;

/**
 * Created by Ruslan on 30-10-2015.
 */
public class Splitter {

    public Splitter() {
    }

    public static String splitCamelCase(String text) {
        return text.replaceAll("(\\p{Ll})(\\p{Lu})", "$1 $2");
    }
}
