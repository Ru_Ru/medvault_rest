package com.ruruapps.medvault.helper;

import com.ruruapps.medvault.domain.Medication;
import com.ruruapps.medvault.domain.Prescription;
import com.ruruapps.medvault.domain.User;
//import org.springframework.boot.autoconfigure.data.rest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

/**
 * Created by Ruslan on 30/11/2015.
 */

//@Configuration
class RepositoryConfig { //extends SpringBootRepositoryRestMvcConfiguration {

//  //  @Override
//    protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
//        super.configureRepositoryRestConfiguration(config);
//        config.exposeIdsFor(User.class, Medication.class, Prescription.class);
//    }
//
//
//    //@Bean
//    public SpringBootRepositoryRestMvcConfiguration repositoryRestConfigurer() {
//
//        return new SpringBootRepositoryRestMvcConfiguration() {
//            @Override
//            public void configureRepositoryRestConfiguration(
//                    RepositoryRestConfiguration config) {
//                config.exposeIdsFor(Medication.class, Prescription.class);
//            }
//        };
//
//    }
}