package com.ruruapps.medvault.service;

import com.ruruapps.medvault.domain.Medication;
import com.ruruapps.medvault.domain.Prescription;

import java.util.ArrayList;

/**
 * Created by Ruslan on 29/11/2015.
 */

public interface PrescriptionService {
    Prescription save(Prescription model);

    void update(Prescription model, ArrayList<Medication> medications);
    void update(Prescription model, Medication medication);
}
