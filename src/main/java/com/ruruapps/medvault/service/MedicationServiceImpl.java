package com.ruruapps.medvault.service;

import com.ruruapps.medvault.domain.Medication;
import com.ruruapps.medvault.domain.MedicationModel;
import com.ruruapps.medvault.domain.Symptom;
import com.ruruapps.medvault.repositories.MedicationRepository;
import com.ruruapps.medvault.repositories.SymptomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

/**
 * Created by Ruslan on 25-10-2015.
 */
@Service("medicationService")
public class MedicationServiceImpl implements MedicationService {

    @Autowired
    private MedicationRepository medicationRepository;
    @Autowired
    private SymptomRepository symptomRepository;

    @Override
    public Medication save(Medication model) {

        Medication medication = new Medication(
                model.getName(),
                model.getDescription(),
                model.getType(),
                ""
               // model.getQuantity()
                );

        medicationRepository.save(medication);

       // model.getSymptoms().forEach(medication::addSymptom);

        for(Symptom symptom : model.getSymptoms()) {
            medication.addSymptom(symptomRepository.findByName(symptom.getName()));
        }

        medicationRepository.save(medication);

        return medication;
    }



}
