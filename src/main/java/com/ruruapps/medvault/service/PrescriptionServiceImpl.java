package com.ruruapps.medvault.service;

import com.ruruapps.medvault.domain.Medication;
import com.ruruapps.medvault.domain.Prescription;
import com.ruruapps.medvault.repositories.PrescriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;

/**
 * Created by Ruslan on 29/11/2015.
 */

@Service("prescriptionService")
public class PrescriptionServiceImpl implements PrescriptionService {


    @Autowired
    private EntityManager em;
    @Autowired
    private PrescriptionRepository prescriptionRepository;
    @Autowired
    private MedicationService medicationService;


    @Override
    public Prescription save(Prescription model) {

        Prescription prescription = new Prescription(
                model.getName(),
                model.getUser(),
                model.getStartDate());

        prescriptionRepository.save(prescription);

        for (Medication medication : model.getMedications()) {
            prescription.addMedication(medication);
        }
        return prescription;
    }

    @Override
    @Transactional
    public void update(Prescription prescription, ArrayList<Medication> medications) {

        for (Medication med:
             medications) {
            prescription.addMedication(med);
        }
        em.merge(prescription);
    }

    @Override
    @Transactional
    public void update(Prescription prescription, Medication medication) {
        prescription.addMedication(medication);

        em.merge(prescription);
    }
}
