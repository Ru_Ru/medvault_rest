package com.ruruapps.medvault.service;

import com.ruruapps.medvault.domain.Medication;
import com.ruruapps.medvault.domain.MedicationModel;

/**
 * Created by Ruslan on 22/11/2015.
 */
public interface MedicationService {
    Medication save(Medication model);
}
