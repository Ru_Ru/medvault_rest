package com.ruruapps.medvault.controller;

import com.ruruapps.medvault.domain.User;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * Created by Ruslan on 29-10-2015.
 */

@Controller
public class TestController {

//    @RequestMapping(value = "/hello")
//    @ResponseBody
//    public String hello(HttpServletRequest req) {
//        //Application app = ApplicationResolver.INSTANCE.getApplication(req);
//        //return "Hello, " + app.getName();
//    }


    @RequestMapping("/checkuser")
    public Principal user(Principal user) {
        return user;
    }

}
