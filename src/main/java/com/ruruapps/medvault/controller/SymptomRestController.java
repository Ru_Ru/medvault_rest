package com.ruruapps.medvault.controller;

import com.google.common.base.Strings;
import com.ruruapps.medvault.domain.Symptom;
import com.ruruapps.medvault.repositories.SymptomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Ruslan on 20/11/2015.
 */

@RestController
@RequestMapping("/api/symptoms")
public class SymptomRestController {

    @Autowired
    private SymptomRepository symptomRepository;

    @RequestMapping(value ="/all", method= RequestMethod.GET)
    public HashMap<String, List<Symptom>> getAll() {

        HashMap<String, List<Symptom>> symptoms = new HashMap<>();

        symptoms.put("symptoms", symptomRepository.findAll());

        return symptoms;
    }
}
