package com.ruruapps.medvault.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ruslan on 09/11/2015.
 */

@RestController
public class LoginController {

    @RequestMapping(value = "/signin", produces = "application/json")
    public Map<String, String> Login(Principal principal) {

        HashMap<String, String> user = new HashMap<>();
        user.put("username", principal.getName());

        return user;
    }

    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }
}
