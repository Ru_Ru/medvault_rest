package com.ruruapps.medvault.controller;

import com.ruruapps.medvault.domain.User;
import com.ruruapps.medvault.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * Created by Ruslan on 30/11/2015.
 */

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/self")
    public HashMap<String, User> getUser() {
        HashMap<String, User> user = new HashMap<>();
        user.put("User", userRepository.findOne((long) 1));

        return user;
    }
}
