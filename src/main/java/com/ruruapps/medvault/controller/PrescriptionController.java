package com.ruruapps.medvault.controller;

import com.ruruapps.medvault.domain.Prescription;
import com.ruruapps.medvault.repositories.PrescriptionRepository;
import com.ruruapps.medvault.service.PrescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.HashMapChangeSet;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ruslan on 25/11/2015.
 */

@RestController
@RequestMapping("/api/prescription")
public class PrescriptionController {

    @Autowired
    private PrescriptionService prescriptionService;
    @Autowired
    private PrescriptionRepository prescriptionRepository;

    @RequestMapping("/all")
    public HashMap<String, ArrayList<Prescription>> getAllPrescriptions() {

        HashMap<String, ArrayList<Prescription>> prescriptions = new HashMap<>();
        prescriptions.put("prescriptions", (ArrayList<Prescription>) prescriptionRepository.findAll());

        return prescriptions;
    }

}
