package com.ruruapps.medvault.controller;

import com.ruruapps.medvault.domain.Medication;
import com.ruruapps.medvault.domain.MedicationModel;
import com.ruruapps.medvault.helper.Splitter;
import com.ruruapps.medvault.repositories.MedicationRepository;

import com.ruruapps.medvault.service.MedicationService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@RestController
@RequestMapping("/api/medication")
public class MedicationController {

    //private static final String PATIENT = "PATIENT";

    @Autowired
    private MedicationRepository medicationRepository;
    @Autowired
    private MedicationService medicationService;

    @RequestMapping(value = "/search/all")
    @ResponseBody
    //@PreAuthorize("hasRole('" + PATIENT + "')")
    List<Medication> allMedications() throws NotFoundException {

        ArrayList<Medication> medications = (ArrayList<Medication>) this.medicationRepository.findAll();
        if (medications == null) {
            throw new NotFoundException("Medications not found");
        }
        return medications;
    }

    @RequestMapping(value = "/all")
    @ResponseBody
    public HashMap<String, List<Medication>> getMedicationMap(){
        HashMap<String, List<Medication>> medications = new HashMap<>();
        medications.put("medications", this.medicationRepository.findAll());
        return medications;
    }


    @RequestMapping(value = "/{id}/details")
    //@PreAuthorize("hasRole('" + PATIENT + "')")
    Medication allMedications(@PathVariable Long id) {

        return this.medicationRepository.findOne(id);
    }

    @RequestMapping(value = "/search/type/{type:(?:[A-Z\\s]\\w+)}", method = RequestMethod.GET)
    //@PreAuthorize("hasRole('" + PATIENT + "')")
    List<Medication> allByType(@PathVariable String type) {

        Pattern pattern = Pattern.compile("(\\p{Ll})(\\p{Lu})");
        Matcher matcher = pattern.matcher(type);
        boolean checkType = matcher.find();

        if (checkType) {
            return this.medicationRepository.findByType(Splitter.splitCamelCase(type));
        } else
            return this.medicationRepository.findByType(type);
    }

    @RequestMapping(value = "/search/symptom/{symptom:(?:[A-Z\\s]\\w+)}", method = RequestMethod.GET)
    //@PreAuthorize("hasRole('" + PATIENT + "')")
    List<Medication> allBySymptom(@PathVariable String symptom) {

        Pattern pattern = Pattern.compile("(\\p{Ll})(\\p{Lu})");
        Matcher matcher = pattern.matcher(symptom);
        boolean checkSymptom = matcher.find();

        if (checkSymptom) {
            return this.medicationRepository.findBySymptom(Splitter.splitCamelCase(symptom));
        } else
            return this.medicationRepository.findBySymptom(symptom);
    }

    @RequestMapping(value = "/create", consumes = "application/json", method = RequestMethod.POST)
    public ResponseEntity<Medication> createMedication(@RequestBody Medication model) {

        //System.out.println(medication.toString());
        Medication medication = new Medication();
        if (model!= null) {
          medication = medicationService.save(model);
        }
        return new  ResponseEntity<>(medication, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete", consumes = "application/json", method = RequestMethod.POST)
    public ResponseEntity deleteMedication(@RequestBody int id) {

        if (id != 0) {
            medicationRepository.delete((long) id);
        }
        return new  ResponseEntity(HttpStatus.OK);
    }
}
