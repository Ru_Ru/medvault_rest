package com.ruruapps.medvault.domain;

import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Created by Ruslan on 25-10-2015.
 */

@Entity
@Table(name = "medication")
public class Medication extends BaseEntity {

    private String name;
    @ManyToMany(fetch = FetchType.EAGER, cascade =
            {CascadeType.MERGE})
    @JoinTable(name = "medication_symptom", joinColumns={
            @JoinColumn(name="medications_id", nullable=false)    }
            , inverseJoinColumns={@JoinColumn(name="symptoms_id", referencedColumnName="id", nullable=false)})
   // @JsonManagedReference("medication_symptom")
    private List<Symptom> symptoms;
    private String type;
    @Column(nullable = true)
    private String img;
    @Column(nullable = true)
    private String description;
//    @Column(nullable = true)
//    private int quantity;

    public Medication() {
    }

    public Medication(String name, List<Symptom> symptoms, String type, String img, String description) {
        this.name = name;
        this.symptoms = symptoms;
        this.type = type;
        this.description = description;
        this.img = img;
        //this.quantity = quantity;
    }

    public Medication(String name, String type, String img, String description) {
        this.name = name;
        this.type = type;
        setImg(img);
        this.description = description;
        //this.quantity = quantity;
    }

//    public Medication(String name, String type, String img, String description) {
//        this.name = name;
//        this.type = type;
//        setImg(img);
//        this.description = description;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Symptom> getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(List<Symptom> symptoms) {
        this.symptoms = symptoms;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        byte[] byteImg = img.getBytes(StandardCharsets.UTF_8);
        this.img = Base64.getEncoder().encodeToString(byteImg);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addSymptom(Symptom symptom) {
        if (getSymptoms() == null) {
            this.symptoms = new ArrayList<>();
            symptoms.add(symptom);
            //symptom.addMedication(this);
        } else if (getSymptoms().contains(symptom)){
            System.out.println("Symptom is already part of this medication");
        }
        else
            symptoms.add(symptom);
    }

//    public int takeMed() {
//        if (quantity > 0) {
//            return this.quantity -1;
//        }
//        return 0;
//    }
//
//    public String checkQuanitity() {
//        if (quantity > 0) {
//            return "Medication amount: " + quantity;
//        }
//        else {
//            return "Your medications have runned out";
//        }
//    }

    @Override
    public String toString() {
        return "Medication{" +
                "name='" + name + '\'' +
                ", symptoms=" + symptoms +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
