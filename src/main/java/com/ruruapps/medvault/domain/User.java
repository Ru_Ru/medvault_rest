package com.ruruapps.medvault.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.core.json.PackageVersion;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ruslan on 25-10-2015.
 */

@Entity
@Table(name="\"User\"")
public class User extends BaseEntity {

    private String name;
    private LocalDate birthday;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "prescription_id")
    @JsonManagedReference
    private List<Prescription> prescriptions;

    public User() {
    }

    public User(String name, LocalDate birthday, List<Prescription> prescription) {
        this.name = name;
        this.birthday = birthday;
        this.prescriptions = prescription;
    }

    public User(String name, LocalDate birthday) {
        this.name = name;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday.toString();
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public List<Prescription> getPrescription() {
        return prescriptions;
    }

    public void setPrescription(List<Prescription> prescription) {
        this.prescriptions = prescription;
    }

    public void addPrescription(Prescription prescription) {
        if (getPrescription() == null) {
           this.prescriptions = new ArrayList<>();
            prescriptions.add(prescription);
        }
        else if (getPrescription().contains(prescription)){
            System.out.println("Prescription of the same kind if already assingned");
        }
        else {
            getPrescription().add(prescription);
        }
    }
}
