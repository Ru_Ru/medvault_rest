package com.ruruapps.medvault.domain;

import org.springframework.data.rest.core.config.Projection;

import java.util.List;

/**
 * Created by Ruslan on 02/12/2015.
 */

@Projection(name = "ModelDetails", types = {Prescription.class})
public interface PrescriptionDetails {
    Long getId();
    String getName();
    String getDescription();
    List<Medication> getMedications();
    User getUser();
    String getStartDate();
    String getEndDate();
    boolean isActive();
}
