package com.ruruapps.medvault.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Entity
@Table(name = "prescription")
public class Prescription extends BaseEntity {

    private String name;
    @ManyToOne
    @JsonBackReference
    private User user;
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @JoinTable(name = "prescription_medication", joinColumns = {
            @JoinColumn(name = "prescription_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "medications_id", referencedColumnName = "id",
                    nullable = false)})
    // @JsonManagedReference
    private List<Medication> medications;
    private boolean isActive;
    private String startDate;
    private String endDate;

    public Prescription() {

    }

    public Prescription(String name, User user, List<Medication> medications, LocalDate now) {
        this.name = name;
        this.user = user;
        this.medications = medications;
        this.isActive = true;
        //setStartDate(now);
        startDate = now.toString();
        setEndDate();
    }

    public Prescription(String name, User user, String now) {
        this.name = name;
        this.user = user;
        this.isActive = true;
        startDate = now;
        setEndDate();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Medication> getMedications() {
        return medications;
    }

    public void addMedication(Medication medication) {
        if (getMedications() == null) {
            this.medications = new ArrayList<>();
            medications.add(medication);
            //medication.addToPrescription(this);
        } else {
            getMedications().add(medication);
            // medication.addToPrescription(this);
        }
    }

    public void addMedications(List<Medication> medicationList) {
        if (getMedications() == null) {
            this.medications = new ArrayList<>();
            for (Medication m : medicationList) {
                medications.add(m);
                //m.addToPrescription(this);
            }
        } else {
            for (Medication m : medicationList) {
                getMedications().add(m);
                // m.addToPrescription(this);
            }
        }
    }

    public void setMedications(List<Medication> medications) {
        this.medications = medications;
    }

    public boolean isAtive() {
        return isActive;
    }

    public void setIsAtive(boolean status) {
        this.isActive = status;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String now) {
        LocalDate StartDate = LocalDate.parse(now);
        //this.startDate = LocalDate.now(ZoneId.of("Europe/Copenhagen"));
        //startDate.format(DateTimeFormatter.ofPattern("dd/MM/YYYY"));
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate() {
        final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        Period period = Period.ofWeeks(2);
        LocalDate EndDate = LocalDate.parse(getStartDate());
        this.endDate = LocalDate.from(EndDate.plus(period)).toString();
    }
}
