package com.ruruapps.medvault.domain;

import org.springframework.data.rest.core.config.Projection;

import java.util.List;

/**
 * Created by Ruslan on 02/12/2015.
 */

@Projection(name = "modelDetail", types = {User.class})
public interface UserDetail {
    Long giveId();
    String getName();
    String getBirthday();
    List<Prescription> getPrescription();
}
