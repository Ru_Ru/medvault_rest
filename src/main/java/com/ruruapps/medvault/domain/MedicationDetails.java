package com.ruruapps.medvault.domain;

import org.springframework.data.rest.core.config.Projection;

import java.util.List;

/**
 * Created by Ruslan on 02/12/2015.
 */

@Projection(name = "ModelDetail", types = {Medication.class})
public interface MedicationDetails {
    Long getId();
    String getName();
    String getType();
    String getImg();
    String getDescription();
    List<Symptom> getSymptoms();
}
