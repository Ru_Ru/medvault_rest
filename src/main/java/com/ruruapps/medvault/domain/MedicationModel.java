package com.ruruapps.medvault.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ruslan on 22/11/2015.
 */

public class MedicationModel implements Serializable {

    private Medication medication;
    private List<Symptom> symptoms;


    public MedicationModel(Medication medication, List<Symptom> symptoms) {
        this.medication = medication;
        this.symptoms = symptoms;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public List<Symptom> getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(List<Symptom> symptoms) {
        this.symptoms = symptoms;
    }
}

