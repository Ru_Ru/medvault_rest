package com.ruruapps.medvault.domain;

import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Created by Ruslan on 27-10-2015.
 */


@MappedSuperclass
public abstract class BaseEntity extends ResourceSupport {

    @JsonProperty("id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    public void setId(Long id) {
        this.id = id;
    }

    public Long giveId() {
        return id;
    }

    public boolean isNew() {
        return (this.id == null);
    }
}