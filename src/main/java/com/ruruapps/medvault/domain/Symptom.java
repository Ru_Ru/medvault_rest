package com.ruruapps.medvault.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreType;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Ruslan on 25-10-2015.
 */

@Entity
@Table
public class Symptom {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String name;
    @Column(nullable = true)
    private String info;
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "symptoms", cascade =
            {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonIgnore
    //@JsonBackReference("medication_symptom")
    private List<Medication> medications;
    public Symptom(String name) {
        this.name = name;
    }

    public Symptom() {
    }

    public Symptom(String name, String info, List<Medication> medications) {
        this.name = name;
        this.info = info;
        this.medications = medications;
    }

    public Symptom(String name, String info) {
        this.name = name;
        this.info = info;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }


    public List<Medication> getMedications() {
        return medications;
    }

    //@JsonBackReference("medication_symptom")
    public void setMedications(List<Medication> medications) {
        this.medications = medications;
    }

    public void addMedication(Medication medication) {
        if (getMedications().isEmpty()) {
            this.medications = new ArrayList<>();
            medications.add(medication);
            //medication.addSymptom(this);
        }
        else {
            getMedications().add(medication);
        }
    }
}
