package com.ruruapps.medvault.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Ruslan on 27-10-2015.
 */


public class Role extends BaseEntity implements GrantedAuthority {

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "username")
    @JsonBackReference
    private User user;
    @NotNull
    private String role;

    public Role(String role) {
        this.role = role;
    }

    public Role() {
    }

    public Role(User user, String authority) {
        this.user = user;
        this.role = authority;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setAuthority(String authority) {
        this.role = authority;
    }

    @Override
    public String getAuthority() {
        return role;
    }
}
